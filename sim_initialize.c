
/* initalize dimensions */
static void
sim_initialize_lw(sim_t *sim)
{
	int i;

	struct {
		double l, aspect_ratio;
	} types [] = {
		{3.20, 2.0},
		{3.90, 2.3},
		{4.25, 2.4},
		{4.55, 2.5},
		{4.60, 2.6},
		{5.15, 2.8},
	};

	for (i=0; i < sim->n; i++) {
		int draw = (int)SAMPLE_UNIFORM(0, 5);
		if (sim->lanedrop && i == 0) {
			sim->l[i] = 0.25 * sim->roadlen_meters;
			sim->w[i] = 3.0;
		} else if (sim->barrier && i == sim->n-1) {
			sim->l[i] = 2.5;
			sim->w[i] = sim->roadwid_meters;
		} else {
			sim->l[i] = types[draw].l;
			sim->w[i] = sim->l[i] / types[draw].aspect_ratio;
			if (SAMPLE_UNIFORM(0, 100) < BUS_RATIO) {
				sim->l[i] = SAMPLE_UNIFORM(12, 17);
				sim->w[i] = 2.5;
				sim->truck[i] = 1;
			}
		}
	}
}

/* initialize positions */
static void
sim_initialize_xy(sim_t *sim)
{
	const double lanewidth = LANEWIDTH;
	const int numlanes = round(sim->roadwid_meters / lanewidth);
	int i, l, j;
	int *lastcar = calloc(sizeof(int), numlanes);

	for (l=0; l < numlanes; l++)
		lastcar[l] = -1;
 	
	for (i=0, l=0; i < sim->n; i++, l++) {
		if (l == numlanes)
			l = 0;
		j = lastcar[l];
		sim->x[i][0] = ((j == -1)?0:sim->x[j][0]) + 0.5*(sim->l[i]+sim->l[j]) + XBUFFER;
		sim->y[i][0] = l*lanewidth + 0.5 + 0.5*sim->w[i];
 		lastcar[l] = i;
	}
	
	free(lastcar);
 }

/* initalize desired speeds */
static void
sim_initialize_vd(sim_t *sim)
{
	int i;
	const int numlanes = round(sim->roadwid_meters/LANEWIDTH);

	for (i=0; i < sim->n; i++) {
		if (sim->lanedrop && i == 0) {
			sim->vx[i][0] = 0;
			sim->vd[i] = 0;
		} else {
			/* i's desired speed in m/s */
			if (sim->single_lane && sim->single_lane_front_vd > 0 && i == sim->n-1) {
				sim->vdx_effective[i] = sim->vd[i] = sim->single_lane_front_vd;
			} else {
				int l = round(sim->y[i][0]/LANEWIDTH);
				sim->vd[i] = sim->vd_meters_per_sec_lo + pow((l+1.0)/numlanes, 2)*(sim->vd_meters_per_sec_hi-sim->vd_meters_per_sec_lo);
				sim->vd[i] = ceil(sim->vd[i]) + VD_VARIANCE * random()/RAND_MAX;
				sim->vdy_effective[i] = 0;
			}
			if (sim->zero_initial_speed)
				sim->vx[i][0] = 0;
			else
				sim->vx[i][0] = 0.5*(sim->vd_meters_per_sec_lo + sim->vd_meters_per_sec_hi);
		}
	}
}

static void
sim_initialize(sim_t *sim) {
	int i;
	assert((sim->x = calloc(sizeof(double*), sim->n)));
	assert((sim->y = calloc(sizeof(double*), sim->n)));

	assert((sim->vx = calloc(sizeof(double*), sim->n)));
	assert((sim->vy = calloc(sizeof(double*), sim->n)));

	assert((sim->ux = calloc(sizeof(double*), sim->n)));
	assert((sim->uy = calloc(sizeof(double*), sim->n)));

	assert((sim->fx = calloc(sizeof(double*), sim->n)));
	assert((sim->fy = calloc(sizeof(double*), sim->n)));


	assert((sim->walls = calloc(sizeof(walls_t*), sim->n)));


	assert((sim->wall_y_up = calloc(sizeof(double*), sim->n)));
	assert((sim->wall_y_dn = calloc(sizeof(double*), sim->n)));
	assert((sim->wall_y_up_j = calloc(sizeof(double*), sim->n)));
	assert((sim->wall_y_dn_j = calloc(sizeof(double*), sim->n)));

	assert((sim->crash = calloc(sizeof(int*), sim->n)));
	assert((sim->flow = calloc(sizeof(double), sim->K)));
	assert((sim->reg = calloc(sizeof(int), sim->K)));


	assert((sim->l = calloc(sizeof(double), sim->n)));
	assert((sim->w = calloc(sizeof(double), sim->n)));
	assert((sim->vd = calloc(sizeof(double), sim->n)));
	assert((sim->truck = calloc(sizeof(int), sim->n)));
	assert((sim->vdx_effective = calloc(sizeof(double), sim->n)));
	assert((sim->vdy_effective = calloc(sizeof(double), sim->n)));

	assert((sim->degree_x = calloc(sizeof(double**), sim->n)));
	assert((sim->degree_y = calloc(sizeof(double**), sim->n)));

	for (i=0; i < sim->n; i++) {
		assert((sim->x[i] = calloc(sizeof(double), sim->K)));
		assert((sim->y[i] = calloc(sizeof(double), sim->K)));

		assert((sim->vx[i] = calloc(sizeof(double), sim->K)));
		assert((sim->vy[i] = calloc(sizeof(double), sim->K)));

		assert((sim->ux[i] = calloc(sizeof(double), sim->K)));
		assert((sim->uy[i] = calloc(sizeof(double), sim->K)));

		assert((sim->fx[i] = calloc(sizeof(double), sim->K)));
		assert((sim->fy[i] = calloc(sizeof(double), sim->K)));

		assert((sim->walls[i] = calloc(sizeof(walls_t), sim->K)));

		assert((sim->wall_y_up[i] = calloc(sizeof(double), sim->K)));
		assert((sim->wall_y_dn[i] = calloc(sizeof(double), sim->K)));
		assert((sim->wall_y_up_j[i] = calloc(sizeof(double), sim->K)));
		assert((sim->wall_y_dn_j[i] = calloc(sizeof(double), sim->K)));

		assert((sim->crash[i] = calloc(sizeof(int), sim->K)));

#if(PRINT_XY_DEGREES)
		assert((sim->degree_x[i] = calloc(sizeof(double*), sim->K)));
		assert((sim->degree_y[i] = calloc(sizeof(double*), sim->K)));
		int t;
		for (t=0; t < sim->K; t++) {
			assert((sim->degree_x[i][t] = calloc(sizeof(double), sim->n)));
			assert((sim->degree_y[i][t] = calloc(sizeof(double), sim->n)));
		}
#endif

	}

	sim_initialize_lw(sim);
	sim_initialize_xy(sim);
	sim_initialize_vd(sim);

#if 0

	// crash resolution period
	do {
		int j, crash_found, crash_resolution_round = 0;
		double ratio_backup = sim->push_repel_ratio;

		sim->warmup = 1;
		sim->push_repel_ratio = 1.1;

 		do {
			crash_found = 0;
			for (i=0; i < sim->n; i++) {
				for (j=0; j < i; j++) {
					if (crash(sim, i, j, 0)) {
						crash_found = 1;
					}
				}
			}
 			sim_run(sim, 2);

			for (i=0; i < sim->n; i++) {
				sim->x[i][0] = sim->x[i][1];
				sim->y[i][0] = sim->y[i][1];
				sim->vx[i][0] = sim->vy[i][0] = sim->ux[i][0] = sim->uy[i][0] = 0;
			}

			crash_resolution_round++;
			fprintf(stderr, "crash resolution rounds: %d\r", crash_resolution_round);
 		} while(crash_found);
		fprintf(stderr, "\n");

		for (i=0; i < sim->n; i++)
			memset(sim->crash[i], 0, sizeof(int)*sim->K);

		sim->warmup = 0;
		sim->push_repel_ratio = ratio_backup;

	}while(0);
#endif
}
