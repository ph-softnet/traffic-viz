\section{Discrete simulation}
	During a time-step of length $T$, 
	each vehicle departs from a point $(x, y)$ with initial horizontal (vertical) speed $v_x$ ($v_y$)
	and reaches a point $(x', y')$ with final horizonal (vertical) speed $v'_x$ ($v'_y$),
	through constant horizontal (vertical) acceleration $f_x$ ($f_y$),
	according to the so-called SUVAT equations of motion:
	\begin{align}
		x' & = x + v_x T + \frac{1}{2} f_x T^2 \label{eq-suvat-1} \\
		y' & = y + v_y T + \frac{1}{2} f_y T^2 \label{eq-suvat-2} \\
		v_x' & = v_x + f_x T \label{eq-suvat-3} \\
		v_y' & = v_y + f_y T \label{eq-suvat-4}
	\end{align}

	A system with $n$ vehicles is simulated according to Algorithm~\ref{alg-sim}.

	\begin{algorithm}
		\begin{algorithmic}[1]
			\FOR{each vehicle $j \in \{1,\ldots,n\}$}
				\STATE set current state $s = (x, y, v_x, v_y)$ to initial values
			\ENDFOR

			\FOR{each vehicle $j \in \{1,\ldots,n\}$}
				\FOR{$t=1,2,\ldots,K-1$}
					\STATE derive controls $(f_x, f_y)$ at current state $s$ from (\ref{eq-fx}, \ref{eq-fy})
					\STATE regulate controls $(f_x, f_y)$ (see \ref{sec:effective})
					\STATE derive next state $s' = (x', y', v'_x, v'_y)$ from (\ref{eq-suvat-1}--\ref{eq-suvat-4})
					\STATE set current state $s \leftarrow s'$
				\ENDFOR
			\ENDFOR
		\end{algorithmic}
		\caption{Simulation of $n$ vehicles and $K$ time-steps of length $T$.}
		\label{alg-sim}
	\end{algorithm}

\section{Deriving controls $(f_x, f_y)$ at the current state}
	\label{sec:controls}
	A vehicle transitions from state $s$ to state $s'$
	according to a vertical and a horizontal push-pull acceleration force, defined as follows: 
	\begin{align}
		f_x = \ats_x \fts_x + \aca_x \fca_x + \ang_x \fng_x \label{eq-fx}\\
		f_y = \ats_y \fts_y + \aca_y \fca_y + \ang_y \fng_y \label{eq-fy}
	\end{align}

	Each of $\fts, \fca$ and $\fng$ is a function of $s$, serving a specific purpose:
	\begin{itemize}
		\item target-speed force $\fts$ ensures a vehicle attains its desired horizontal (vertical) target speed;
		\item collision-avoidance force $\fca$ prevents vehicle-to-vehicle collisions;
		\item nudging force $\fng$ enables faster upstream vehicles (i.e. with a higher target speed) to drive through by pushing slower vehicles aside as necessary.
	\end{itemize}

	\subsection{Target-speed force $\vec{\fts} = \fts_x + j \fts_y$}

	\subsubsection{Horizontal componet}
		A possible definition for horizontal component of the target-speed force 
		(and the one used in the current implementation) is given below: 
		\begin{align}
			\fts_x(v_x) = \widetilde{\max}\left\{u^-, u^+ \left[1 - e^{-z(v^d_x - v_x)} \right]\right\}
		\end{align}
		Here, $v^d_x - v_x$ is the deviation of current ($v_x$) from target ($v^d_x$) speed;
		$u^- < 0$ and $u^+ > 0$ denote the maximum applicable deceleration and acceleration, respectively, and $z$ is a smoothening factor.
		Moreover, $\widetilde{\max}$ denotes a smoothened maximum operator, defined as follows:
		\begin{align}
			\widetilde{\max}\{o_1, o_2, \ldots, o_n\} = \log(e^{o_1} + e^{o_2} + \ldots + e^{o_n})
		\end{align}
		A plot of this function with indicative parameters is shown in Figure~\ref{fig-fts-x}.

		\begin{figure}
			\centering
			\begin{subfigure}[t]{0.45\textwidth}
				\includegraphics[width=0.95\textwidth]{fts_x}
				\caption{Horizontal target-speed force component with $v^d_x = 30 m/s, u^- = -3 m/s^2, u^+ = 4 m/s^2$.}
				\label{fig-fts-x}
			\end{subfigure}
			\qquad
			\begin{subfigure}[t]{0.45\textwidth}
				\includegraphics[width=0.95\textwidth]{fts_y}
				\caption{Vertical target-speed force component with $\bar{u} = 2 m/s^2$.}
				\label{fig-fts-y}
			\end{subfigure}
			\caption{Target-speed force components.}
		\end{figure}

	\subsubsection{Vertical component}
		Along the vertical axis, the purpose of $\fts_y$ is to counter any deviation of vertical speed from zero.
		As such, it is defined as follows:
		\begin{align}
			\fts_y(v_y) = \bar{u} \left[\frac{2}{1 + e^{-z(0 - v_y)}} - 1\right]
		\end{align}
		Here $\bar{u}$ is the maximum applicable vertical (de)aceleration and $z$ a smoothening parameter.
		An indicative plot is given in Figure~\ref{fig-fts-y}.
		Note that in contrast with $\fts_x$, this force acts symmetrically above and below target speed.

	\subsection{Collision-avoidance and nudging forces}
		In our model, each vehicle is associated with an artificial "aura" which encloses it,

		\subsubsection{The direction of collision-avoidance and nudge-like forces}
			Consider the situation illustrated in Figure~\ref{fig-fca},
			with a vehicle $i$ positioned at an angle $\theta$ within an artificial ``aura'',
			which encloses a following vehicle $j$.
			As will be detailed in Section~\ref{sec-fca-mag},
			the shape of this aura depends on the dimensions of vehicles $i$ and $j$ but also the velocity of the enclosed vehicle $j$.
			The purpose of this aura is to facilitate the definition of collision-avoidance and nudge-like forces, as described in the following.

			\begin{figure}
				\centering
				\includegraphics[width=0.4\textwidth]{fca}
				\caption{Collision-avoidance and nudge-like forces.}
				\label{fig-fca}
			\end{figure}

			When $i$ is positioned within the aura enclosing $j$, 
			a collision-avoidance force $\vec{\fca_{i}}$ is excerted onto $j$, pushing it away from $i$.
			At the same time, a ``nudge''-like force $\vec{\fng_{j}}$ is excerted onto $i$, pushing it away from $j$.
			Effectively, then, $j$ is subject to the sum $\sum_i \vec{\fca_i}$ of collision-avoidance forces induced by sufficiently close downstream vehicles $i$,
			while $i$ is subject to the sum $\sum_j \vec{\fca_j}$ of such nudge-like forces, induced by upstream vehicles $j$ following closely.

			These two forces have an equal magnitude (i.e $|\fca_i| = |\fca_j|$) which is determined 
			by the shape of this aura and the positioning of $i$ with respect to $j$,
			as described in the following section.

		\subsubsection{The magnitude of collision-avoidance and nudge-like forces}
			\label{sec-fca-mag}

			For each pair of vehicles $i$ and $j$ positioned as illustrated in Figure~\ref{fig-fca},
			let us define an aura enclosing $j$, the shape and location of which is determined by the following function:
			\begin{align}
				g_{ij}(x, y) := \left[\frac{1}{1 + \left(\frac{x-x_j}{a^x_{ij}}\right)^2 + \left(\frac{y-y_j}{a^y_{ij}}\right)^2}\right]^2
			\end{align}
			with $(x_j, y_j)$ representing the position of $j$ (i.e. its center).
			Parameters $a^x_{ij}, a^y_{ij}$ depend on:
			\begin{itemize}
				\item the dimensions (i.e. length and width) of vehicles $i$ and $j$;
				\item the velocity of the enclosed vehicle $j$.
			\end{itemize}
			Intuitively, function $g_{ij}: \mathbb{R}^2 \rightarrow [0, 1]$ represents the 
			extent to which vehicle $i$ is an obstacle that should be avoided by $j$.

			When $j$ is still (not moving in any direction), $a^x_{ij}$ equals the half-length $l_j/2$ of vehicle $j$ plus that of vehicle $i$ (i.e. $a^x_{ij} = \frac{1}{2}(l_j + l_i)$).
			Similarly, $a^y_{ij}$ equals the half-width $w_j/2$ of vehicle $j$ plus that of vehicle $i$ (i.e. $a^y_{ij} = \frac{1}{2}(w_j + w_i)$).
			For the particular case of $j$ being still, indicative plots of $g_{ij}$ are given in Figure~\ref{gij-vx-0-vy-0}.

			\begin{figure}
				\centering
				\begin{subfigure}[b]{0.48\textwidth}
					\includegraphics[width=0.98\textwidth]{gij-vx-0-vy-0}
					\caption{Contours.}
				\end{subfigure}
				\quad
				\begin{subfigure}[b]{0.48\textwidth}
					\includegraphics[width=0.98\textwidth]{gij-vx-0-vy-0-cross-both}
					\caption{Cross-section profiles.}
				\end{subfigure}
				\caption{Aura $g_{ij}$ assuming $j$ is positioned at $(0,0)$ remaining still and with vehicle dimensions $l_j=5, l_i=4, w_j = w_i = 2$.}
				\label{gij-vx-0-vy-0}
			\end{figure}

			As the reader can easily verify, the ellipsoid defined by $g_{ij}$ is centered at $(0,0)$ where it reaches its maximum value ($g_{ij}(0,0) = 1$).
			Moreover, $g_{ij}$ gradually fades to zero over an area of length $\frac{1}{2}(l_j+l_i)$ and width $\frac{1}{2}(w_j+w_i)$ in each quadrant.
			
			When $j$ is moving, its aura is extended in proportion to its speed in the direction of movement,
			according to a two-dimensional functional equivalent of the constant time-gap,
			determined by a horizontal constant component $\mathcal{T}_x$ and a vertical component $\mathcal{T}_y$.
			Figure~\ref{gij-non-still} illustrates this effect when $j$ is moving with indicative non-zero speeds and letting $\mathcal{T}_x = \mathcal{T}_y = 0.5$.

			\begin{figure}
				\centering
				\begin{subfigure}[b]{0.48\textwidth}
					\includegraphics[width=0.98\textwidth]{gij-vx-20-vy-0}
					\caption{$g_{ij}$ with $v^j_x = 20 m/s, v^j_y = 0 m/s$.}
				\end{subfigure}
				\quad
				\begin{subfigure}[b]{0.48\textwidth}
					\includegraphics[width=0.98\textwidth]{gij-vx-20-vy-5}
					\caption{$g_{ij}$ with $v^j_x = 20 m/s, v^j_y = 5 m/s$.}
				\end{subfigure}
				\caption{Aura $g_{ij}$ as in Figure~\ref{gij-vx-0-vy-0} 
							but with non-zero horizontal and/or vertical velocities for $j$, and with $\mathcal{T}_x = \mathcal{T}_y = 0.5$.}
				\label{gij-non-still}
			\end{figure}

			Making $g_{ij}$ behave as described so far is accomplished by defining $a^x_{ij}$ and $a^y_{ij}$ as follows:
			\begin{align}
				a^x_{ij} & = 
				\begin{cases}
					\frac{1}{2}(l_j+l_i) + \mathcal{T}_x \cdot v^j_x 	&	x \geq x_{ij}, \\
					\frac{1}{2}(l_j+l_i) &	x < x_{ij}
				\end{cases} 
			\end{align}
					
			\begin{align}
				a^y_{ij} & = 
				\begin{cases}
					a^{y,+}_{ij} 	&	v^j_y \geq 0, \\
					a^{y,-}_{ij} 	&	v^j_y < 0
				\end{cases} 
			\end{align}
			where
			\begin{align}
				a^{y,+}_{ij} & = 
				\begin{cases}
					\frac{1}{2}(w_j+w_i) + \mathcal{T}_y \cdot v^j_y 	&	y \geq y_{ij}, \\
					\frac{1}{2}(w_j+w_i) &	y < y_{ij}
				\end{cases} 
			\end{align}
			and
			\begin{align}
				a^{y,-}_{ij} & = 
				\begin{cases}
					\frac{1}{2}(w_j+w_i) &	y \geq y_{ij}, \\
					\frac{1}{2}(w_j+w_i) + \mathcal{T}_y \cdot |v^j_y| 	&	y < y_{ij}, \\
				\end{cases} 
			\end{align}

\section{Road boundaries and other hard constraints}
\label{sec:effective}
	
	As specified earlier with (\ref{eq-fx}) and (\ref{eq-fy}),
	acceleration forces $f_x, f_y$ acting on each vehicle are computed as a linear combination of component forces described in Section~\ref{sec:controls}.
	Before using them to transiton to the next state, however, 
	$f_x$ and $f_y$ are regulated in order to enforce the following hard constraints:
	\begin{enumerate}
		\item $\bar{u}^{\min}_x \leq f_x \leq \bar{u}^{\max}_x$ and $|f_y| \leq \bar{u}_y$;
		\item no vehicle may cross the road-boundaries in the $y$ direction.
	\end{enumerate}
	Regarding constraint 1, constant parametres $\{\bar{u}^{\min}_x, \bar{u}^{\max}_x\}$ and $\bar{u}_y$ specify technological 
	acceleration limitations in the horizontal and lateral direction, respectively.
	Constraint 2 asks that no vehicle will exit the road,
	and the remainder of this section is devoted to the design of this regulatory mechanism.

	The following lemma introduces the \emph{escape velocity bound}, namely $\sqrt{-2 \bar{u} d}$, as a function of
	a vehicle's distance $d$ to the road boundary and the hardest deceleration $\bar{u}$ that can be applied to prevent a collision with the boundary. 
	\begin{lemma}
		Consider a vehicle located at distance $d$ from the road boundary and travelling towards it with speed $v \geq 0$.
		A collision with the boundary is avoidable if and only if $v \leq \sqrt{-2\bar{u} d}$
		where $\bar{u} <0$ is the maximum applicable lateral deceleration.
	\end{lemma}
	Note that the escape velocity bound tends to zero (at a rate that depends on $\bar{u}$) as $d$ tends to zero.
	It can be shown that having the lateral velocity of each vehicle never exceed 
	the escape velocity bound (corresponding to its position at every point in time),
	is a sufficient and necessary condition for ensuring no vehicle exits the road.

	Allowing for a temporary abuse of notation,
	let $d(t), v(t)$ denote the lateral distance and speed towards a road boundary at time $t$.
	Consider a vehicle at distance $d(0)$ and with initial speed $v(0)$ such that $v(0) \leq \sqrt{-2 \bar{u} d(0)}$.
	Applying constant acceleration $u(0)$ for time $T$ results in a distance $d(T)$ and speed $v(T)$, according to Eq. (\ref{eq-suvat-2}) and (\ref{eq-suvat-4}).
	As it follows from the following lemma, bounding $u(0)$ (lateral acceleration towards the boundary) from above by $\bar{U}(d(0),v(0),T)$ 
	is necessary and sufficient to ensure lateral speed remains bounded by escape velocity
	at every intermediate timepoint $t \in [0, T]$.

	\begin{lemma}
        \label{lemm:ubound}
		  The escape velocity bound is satisfied at every $t \in [0, T]$ (i.e. $v(t) \leq \sqrt{-2 \bar{u} d(t)}$) if and only if $u(0) \leq \bar{U}(y(0), v(0), T)$, 
		  with 
		  \begin{align}
			  \bar{U}(d(0), v(0), T) := 
			  \frac{1}{2} \left( \bar{u} -2 T v(0) + \sqrt{\bar{u}^2 - \frac{4 \bar{u} v(0)}{T} + \frac{8 \bar{u}(T v(0) - d(0))}{T^2}} \right)
		  \end{align}
	\end{lemma}

\section{Determining component force coefficients}
	
	\ldots
