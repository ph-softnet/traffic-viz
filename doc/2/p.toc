\contentsline {section}{\tocsection {}{1}{Discrete simulation}}{1}
\contentsline {section}{\tocsection {}{2}{Deriving controls $(f_x, f_y)$ at the current state}}{1}
\contentsline {subsection}{\tocsubsection {}{2.1}{Target-speed force $\mathaccentV {vec}17E{f^{\texttt {ts}}} = f^{\texttt {ts}}_x + j f^{\texttt {ts}}_y$}}{2}
\contentsline {subsection}{\tocsubsection {}{2.2}{Collision-avoidance forces $\mathaccentV {vec}17E{f^{\texttt {ca}}_i} = f^{\texttt {ca}}_{i,x} + j f^{\texttt {ca}}_{i,y}$}}{2}
\contentsline {subsubsection}{\tocsubsubsection {}{2.2.1}{Determining $|f^{\texttt {ca}}_i|$}}{2}
\contentsline {subsection}{\tocsubsection {}{2.3}{Nudging forces $\mathaccentV {vec}17E{f^{\texttt {ng}}_i} = f^{\texttt {ng}}_{i,x} + j f^{\texttt {ng}}_{i,y}$}}{4}
\contentsline {section}{\tocsection {}{3}{Road boundaries and other hard constraints}}{4}
\contentsline {section}{\tocsection {}{4}{Determining component force coefficients}}{4}
\contentsline {section}{\tocsection {}{}{References}}{4}
