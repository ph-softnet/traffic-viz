let conf = 
{
	el: "#app",
	data: {
		sim: window.sim,
		t: 0,
		px_per_meter: 1,
		px_per_meter_max: 10,
		px_per_meter_min: 1,
		d3: {
			data: null,
            data_crashes: null,
            data_progress: 0
		},
        d3_data_progress: 0,
		play: {
			t: 0,
			active: false,
			intvl: null,
			speed: 2
		},
		 
		chart: {
			mode: "general",
			vehicle: {
				id: 0
			},
			general: {
				type: "mdsd"
			}
		}
	},

	methods: {
        road_container_resize(el) {
            let width_px = $(el).width() - 10;
            this.px_per_meter = Math.min(this.px_per_meter_max, Math.max(this.px_per_meter_min, width_px / this.sim.roadlen_meters));
            if (!this.d3.svg_main)
                this.svg_initialize();
            this.svg_show(this.play.t);
        },
        svg_initialize() {
            this.d3.svg_main = d3.select("#svg-main");
            this.d3.g_main = this.d3.svg_main.append("g")
                .attr("id", "g-main")
                .attr("transform", 
                `translate(0, 50) scale(${this.px_per_meter}, ${this.px_per_meter})`);

            // create road rectangle
            this.d3.road_rect = this.d3.g_main.append("rect")
                .attr("stroke", "white")
                .attr("stroke-width", 1.5/this.px_per_meter)
                .attr("x", "0").attr("y", "0")
                .attr("width", this.sim.roadlen_meters)
                .attr("height", this.sim.roadwid_meters)
                .attr("fill", "#1a237e");
            
            this.d3.wall_y_up = this.d3.g_main.append("line")
                .attr("x1", 0).attr("x2", this.sim.roadlen_meters)
                .attr("y1", 0).attr("y2", 0)
                .attr("stroke", "cyan")
                .attr("stroke-width", 2/this.px_per_meter);
            
            this.d3.wall_y_dn = this.d3.g_main.append("line")
                .attr("x1", 0).attr("x2", this.sim.roadlen_meters)
                .attr("y1", this.sim.roadwid_meters).attr("y2", this.sim.roadwid_meters)
                .attr("stroke", "cyan")
                .attr("stroke-width", 2/this.px_per_meter);
            
            this.d3.wall_x = this.d3.g_main.append("line")
                .attr("x1", this.sim.roadlen_meters).attr("x2", this.sim.roadlen_meters)
                .attr("y1", 0).attr("y2", this.sim.roadwid_meters)
                .attr("stroke", "cyan")
                .attr("stroke-width", 2/this.px_per_meter);

            this.svg_load_data();
            this.svg_show(0);
        },
        svg_load_data() {
            let pathmaker = d3.line().curve(d3.curveLinear);

            // create d3 data vector
            this.d3.data = [];
            this.d3.data_crashes = [];
            for (var t=0; t < this.sim.K; t++) {
                this.d3.data_progress = Math.round(((t/this.sim.K) * 100));
                this.d3.data.push([]);
                this.d3.data_crashes.push((t > 0)?this.d3.data_crashes[t-1]: 0);
                var j = 0;
                for (var i=0; i < this.sim.n; i++) {
                    // create data[t][i] = {}
                    this.d3.data[t].push({}); 
                    this.d3.data[t][j].id = j;
                    this.d3.data[t][j].l = this.sim.l[j];
                    this.d3.data[t][j].w = this.sim.w[j];
                    this.d3.data[t][j].vd = this.sim.vd[j];
                    this.d3.data[t][j].x = this.sim.x[j][t];
                    this.d3.data[t][j].y = this.sim.y[j][t];
                    this.d3.data[t][j].vx = this.sim.vx[j][t];
                    this.d3.data[t][j].crash = this.sim.crash[j][t];
                    this.d3.data_crashes[t] += this.d3.data[t][j].crash;

                    this.d3.data[t][j].leader_arc = {
                        x1: this.sim.l[j],
                        x2: this.sim.l[j],
                        y1: 0.5*this.sim.w[j],
                        y2: 0.5*this.sim.w[j],
                        opacity: 0
                    };
                    let lead = this.sim.leader[j][t];
                    if (lead !== -1) {
                        let lead_dx = (this.sim.x[lead][t]-0.5*this.sim.l[lead]) - (this.sim.x[j][t]+0.5*this.sim.l[j]);
                        let lead_dy = (this.sim.y[lead][t]+0.5*this.sim.w[lead]) - (this.sim.y[j][t]+0.5*this.sim.w[j]);
                        if (lead_dx > 0) {
                            this.d3.data[t][j].leader_arc.x2 += lead_dx;
                            this.d3.data[t][j].leader_arc.y2 += lead_dy;
                            this.d3.data[t][j].leader_arc.opacity = 1.0;
                        }
                    }

                    this.d3.data[t][j].vx_mean = (t > 0)?this.d3.data[t-1][j].vx_mean + this.d3.data[t][j].vx: this.d3.data[0][j].vx;
                    if (this.sim.x[j][t] <= 2*this.play.speed*this.sim.vd[j]*this.sim.T || this.sim.x[j][t] >= this.sim.roadlen_meters-2*this.play.speed*this.sim.vd[j]*this.sim.T)
                        this.d3.data[t][j].reset = true;
                    else
                        this.d3.data[t][j].reset = false;
                    j++;
                }
            }

            for (var t=0; t < this.sim.K; t++) {
                for (var i=0; i < this.sim.n; i++) {
                    this.d3.data[t][i].vx_mean /= (t+1);
                }
            }
            this.d3.data_progress = 100;
        },
        svg_show(t) {
            const update = this.d3.g_main
                .selectAll("g.g-veh")
                .data(this.d3.data[t]);

            /* enter is non-empty only at svg_show(0) */

            /* enter.1) create a <g> associated with each datum in this.d3.data[]  */
            const new_g_elements = update.enter()
                    .append("g")
                    .attr("class", "g-veh");

            /* enter.2) insert a <rect> within <g> */
            new_g_elements.append("rect")
                    .attr("stroke", "hotpink")
                    .attr("rx", 1/this.px_per_meter)
                    .attr("ry", 1/this.px_per_meter)
                    .attr("width", d => d.l)
                    .attr("height", d => d.w)
			  			  .on("click", (d, j) => {
							  if (this.chart.vehicle.id === j)
								  this.chart.vehicle.id = null;
							  else 
                                this.chart.vehicle.id = j;
                          });

            /* enter.3) insert a <line> within <g> */
           new_g_elements.append("line")
                .attr("fill", "none")
                .attr("stroke", "white")
                .attr("stroke-width", 0.5/this.px_per_meter);

            /* update.merge(enter) is non-empty for every svg_show(t) */

            /* for each <g>, set vehicle position at t; note: (d.x, d.y) is center position */ 
            let existing_g_elements = update.merge(new_g_elements);

            existing_g_elements.attr("transform", d => `translate(${d.x-0.5*d.l}, ${d.y-0.5*d.w})`);

            /* for each <g> <rect>, set color according to deviation from desired speed */ 
            existing_g_elements.select("rect")
                .attr("fill", (d,i) => {
                    if (i === this.chart.vehicle.id)
					    return "green";

                    if (i == 0 && sim.lanedrop !== 0 || d.crash !== 0)
                        return "black";
                    if (d.vx < 0.95*d.vd) 
                        return chroma.temperature(Math.pow(d.vx/(1+d.vd), 2) * 2500);
                    
                    if (d.vx <= 1.15*d.vd)
                        return "deepskyblue";

                    if (d.vx <= 1.35*d.vd)
                        return "blue";

                    return "purple";
                })
                .attr("stroke-width", (d) => (d.crash !== 0)?1/this.px_per_meter: 0);
    
            /* for each <g> <line>, set coordinates according to leader */
            existing_g_elements.select("line")
                  .attr("x1", (d) => d.leader_arc.x1)
                  .attr("y1", (d) => d.leader_arc.y1)
                  .attr("x2", (d) => d.leader_arc.x2)
                  .attr("y2", (d) => d.leader_arc.y2)
                  .attr("opacity", (d) => d.leader_arc.opacity);

            let j = this.chart.vehicle.id;
            if (j !== null) {

                let wallx = this.sim.wall_x[j][t];
                if (wallx < this.sim.x[j][t])
                    wallx = this.sim.roadlen_meters;
                
                this.d3.wall_x
                    .attr("x1", wallx)
                    .attr("x2", wallx)
                    .attr("y1", this.sim.wall_y_dn[j][t])
                    .attr("y2", this.sim.wall_y_up[j][t]);
                
                this.d3.wall_y_dn
                    .attr("x1", this.sim.x[j][t]-0.5*this.sim.l[j])
                    .attr("x2", wallx)
                    .attr("y1", this.sim.wall_y_dn[j][t])
                    .attr("y2", this.sim.wall_y_dn[j][t]);

                
                this.d3.wall_y_up
                    .attr("x1", this.sim.x[j][t]-0.5*this.sim.l[j])
                    .attr("x2", wallx)
                    .attr("y1", this.sim.wall_y_up[j][t])
                    .attr("y2", this.sim.wall_y_up[j][t]);
                
            }

            update.exit().remove();
        },
        play_start() {
            this.play.active = true;
            this.svg_show(this.play.t);
            this.play.intvl = setInterval(() => {
                if (this.play.t >= this.sim.K-1) {
                    this.play_stop();
                    this.play.t = 0;
                    return;
                }
                this.play.t += 1;
                this.svg_show(this.play.t);
            }, this.sim.T*1000/this.play.speed);
        },
        play_stop() {
            this.play.active = false;
            clearInterval(this.play.intvl);
        },
        chart_show() {
            window.chart_show(this);
        }
	}
};

Vue.use(Vuetify);
new Vue(conf);
