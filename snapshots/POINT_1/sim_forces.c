
#define DEBUGGING 1

static double
circular_dx(sim_t *sim, double dx) {
	double len = sim->roadlen_meters;
	if (fabs(dx) <= 0.5*len)
		return dx;
	else
		return (dx >= 0)?(dx-len):(dx+len);
}

static double
smooth_max(double x1, double x2, double z) {
	assert(z != 0);
	return log(exp(z*x1) + exp(z*x2))/z;
}

static double
smooth_step(double t, double z) {
	return erf(z * t);
	//return 0.5*(sqrt(4*z)*t/sqrt(1 + 4*z*t*t) + 1);
}

static double
smooth_pulse(double t, double a, double b, double z) {
	return smooth_step(t-a, z) - smooth_step(t-b, z);
}
static double
ftsx(sim_t *sim, int i, int t) {
	return smooth_max(sim->ftsx_lo, sim->ftsx_hi*(1 - exp(-sim->ftsx_zeta*(sim->vdx_effective[i]-sim->vx[i][t]))), 1.0);
}

static double
ftsy(sim_t *sim, int i, int t) {
	return sim->ftsy_hi*(2 / (1 + exp(-sim->ftsy_zeta*(sim->vdy_effective[i] - sim->vy[i][t]))) - 1);
}

static double
fmdl(sim_t *sim, int i, int t) {
	double vd_range = (sim->vd_meters_per_sec_hi - sim->vd_meters_per_sec_lo);
	double point_in_range = 0.5;
	double spread_factor = 1.0;
	double gravity_point;
	if (sim->virtual_lanes) {
		point_in_range = (sim->vd[i] - sim->vd_meters_per_sec_lo)/vd_range;
	}
	gravity_point = sim->w[i]/2 + (1 - point_in_range)*(sim->roadwid_meters - sim->w[i])*spread_factor;
	return sim->coeff_fmdl*(2 / (1 + exp(-sim->ftsy_zeta*(gravity_point - sim->y[i][t]))) - 1);
}

static long double
U_lemma3(sim_t *sim, double v, double d, double ubar) {
	double nom, den; 
	double T = sim->T;
	double ret;

	nom = T*ubar- 2*v + sqrt(pow(T,2)*pow(ubar,2)-(4*T*ubar*v)+8*ubar*(T*v-d));
	den = 2*T;

	if (fpclassify((ret = nom/den)) == FP_NAN)
		ret = ubar;

	return ret;
}

static double
fca_mag(sim_t *sim, int i, int j, int t, int j_repels_i) {
	double xi = sim->x[i][t], xj = sim->x[j][t];
	double yi = sim->y[i][t], yj = sim->y[j][t];
	double li = sim->l[i], lj = sim->l[j];
	double wi = sim->w[i], wj = sim->w[j];
	double dx = circular_dx(sim, xj-xi);
	double dy = yj - yi;
	double mag = 0;
	
	// effective time-gaps
	double time_gap_x = sim->time_gap_x;
	double time_gap_y =	sim->time_gap_y;

	switch(sim->fca_method) {
		case FCA_0:
			do {
				double bufferx = time_gap_x * sim->vx[i][t];
				double buffery_up = time_gap_y * MIN(sim->vy[i][t], 0);
				double buffery_dn = time_gap_y * MAX(sim->vy[i][t], 0);

				mag = MIN(
							smooth_pulse(dx,-0.5*(li+lj) - bufferx, +0.5*(li+lj) + bufferx, sim->fcax_zeta), 
							smooth_pulse(dy,-0.5*(wi+wj)+buffery_up, +0.5*(wi+wj)+buffery_dn, sim->fcay_zeta)
						);
			}while (0);
			break;

		case FCA_1:
			do {
				double ax = 0.5*(li+lj);
				double ay = 0.5*(wi+wj);

				//double buffx = time_gap_x*(0.5*pow(sim->vx[i][t],2)/fabs(sim->uxmin_hard/2));
				//double buffy = time_gap_y*(0.5*pow(sim->vy[i][t],2)/fabs(sim->uymax_hard/2));
				double buffx = time_gap_x*sim->vx[i][t];
				double buffy = time_gap_y*fabs(sim->vy[i][t]);


				if (dx >= 0)
					ax += buffx;

				assert(dx >= 0);

				if ((sim->vy[i][t] >= 0 && yj > yi)  || (sim->vy[i][t] < 0 && yj < yi))
					ay += buffy;

				double xterm = pow(dx/ax, 2);
				double yterm = pow(dy/ay, 4);

				mag = 1.0/(1.0 + xterm + yterm);
			} while (0);
			break;

		case FCA_2:
			do {
				double ax = 0.5*(li+lj);
				double ay = 0.5*(wi+wj);

				double bufferx = time_gap_x*(0.5*pow(sim->vx[i][t],2)/fabs(-2));
				double buffery = time_gap_y*(0.5*pow(sim->vy[i][t],2)/fabs(-2));
				//double bufferx = time_gap_x*sim->vx[i][t];
				//double buffery = time_gap_y*fabs(sim->vy[i][t]);

				if (dx >= 0)
					ax += bufferx;

				assert(dx >= 0);

				if ((sim->vy[i][t] >= 0 && yj > yi)  || (sim->vy[i][t] < 0 && yj < yi))
					ay += buffery;

				double xterm = pow(dx/ax, 2);
				double yterm = pow(dy/ay, 4);
				
				mag = MIN(smooth_pulse(dx,-0.5*(li+lj)-bufferx, +0.5*(li+lj)+bufferx, 1), smooth_pulse(dy,-0.5*(wi+wj), +0.5*(wi+wj), 1)) 
					+ 1.0/(1.0 + xterm + yterm);

			} while (0);
			break;
		default:
			break;
	}

	return mag;
}

static int
fca(sim_t *sim, int i, int j, int t, double *fcax, double *fcay) {
	double dx, dy, mag, ang, dy_distortion;
	// i - me 
	// j - the obstacle

	*fcax = *fcay = 0;

	if (fabs((dx = circular_dx(sim, sim->x[j][t] - sim->x[i][t]))) >= sim->influence_radius_meters)
		return 0;

	if (fabs((dy = sim->y[j][t] - sim->y[i][t])) >= sim->roadwid_meters/2)
		return 0;

	if (dx >= 0) {
		// positioning of obstacle j within my aura
		mag = fca_mag(sim, i, j, t, 1);
	} else {
		// positioning of myself within obstacle j's aura
		mag = fca_mag(sim, j, i, t, 0);
		mag *= sim->push_repel_ratio;
	}
	
	mag = MAX(-1, MIN(1, mag));

	dy = sim->y[j][t] - sim->y[i][t];
	dy_distortion = (dx >= 0)?((sim->y[j][t] < sim->roadwid_meters/2)?+0.1:-0.1):0;
	ang = atan2(dy+dy_distortion, dx);

	*fcax = -cos(ang) * mag;
	*fcay = -sin(ang) * mag;

	return 1;
}

static int
crash(sim_t *sim, int i, int j, int t) {
	double dx = circular_dx(sim, sim->x[j][t] - sim->x[i][t]);
	double dy = sim->y[j][t] - sim->y[i][t];

	if (i == j)
		return 0;

	if (fabs(dx) <= 0.5*(sim->l[i]+sim->l[j]) && fabs(dy) <= 0.5*(sim->w[i]+sim->w[j])) {
		if (!sim->warmup) {
			sim->crashes++;
			if (t > 0 && ((sim->leader[i][t] == j) || (sim->leader[j][t] == i)))
				sim->crashes_on_leaders++;
		}
		
		return 1;
	}

	if (t > 0 && sim->ux[i][t-1] <= 0.95*sim->uxmin_hard)
		return 0;

	return 0;
}

static void
determine_forces(sim_t *sim, int i, int t) {
	int j;
	double fcax = 0, fcay = 0;

	/* initalization */
	sim->vdx_effective[i] = MAX(0.1*sim->vd[i], MIN(sim->vd[i], sim->vdx_effective[i]+sim->T));
	sim->vdy_effective[i] = 0.5*sim->vdy_effective[i];
	
 	sim->fx[i][t] = sim->fy[i][t] = 0;

	/* target-speed related forces */
	sim->fx[i][t] += ftsx(sim, i, t);
	sim->fy[i][t] += ftsy(sim, i, t);
	sim->fy[i][t] += fmdl(sim, i, t);

	/* obstacle-related forces */
	double fcax_sum = 0, fcay_sum =0;
	for (j=0; j < sim->n; j++) {
		if (j == i)
			continue;

		if (crash(sim, i, j, t)) {
			sim->crash[i][t] = 1;
			if (!sim->warmup)
				fprintf(stderr, "crash at %d\n", t);
		} else
			sim->crash[i][t] = 0;

		fca(sim, i, j, t, &fcax, &fcay);

		fcax_sum += fcax;
		fcay_sum += fcay;
	}
	sim->fx[i][t] += sim->coeff_fcax * MAX(-1, MIN(1, fcax_sum));
	sim->fy[i][t] += sim->coeff_fcay * MAX(-1, MIN(1, fcay_sum));


	/*
	 * Hard constraints
	 */

	/* y wall */
	#define WALL_DN(point) U_lemma3(sim, -sim->vy[i][t], sim->y[i][t]-0.5*sim->w[i] - (point), -sim->uymax_hard)
	#define WALL_UP(point) U_lemma3(sim, +sim->vy[i][t], (point) - (sim->y[i][t]+0.5*sim->w[i]), -sim->uymax_hard)
	double Udn, Uup;
	if (sim->dynamic_y_walls) {
		Uup = WALL_UP(sim->wall_y_up[i][t]);
		Udn = WALL_DN(sim->wall_y_dn[i][t]);
	}
	if (!sim->dynamic_y_walls || -Udn > Uup)  {
		Uup = WALL_UP(sim->roadwid_meters);
		Udn = WALL_DN(0);
	}
	sim->fy[i][t] = MAX(sim->fy[i][t], -Udn);
	sim->fy[i][t] = MIN(sim->fy[i][t], +Uup);

	/* x wall */
	if (sim->dynamic_x_walls && sim->leader[i][t] != -1) {
		double Ufw_v = MAX(0, sim->vx[i][t] - (1-sim->safety_level)*sim->wall_x_v[i][t]);
		double Ufw = U_lemma3(sim, +Ufw_v, circular_dx(sim, sim->wall_x[i][t] - (sim->x[i][t]+0.5*sim->l[i])), sim->uxmin_hard);
		sim->fx[i][t] = MIN(sim->fx[i][t], Ufw);
	}

	/* [umin, umax] ranges */
	sim->fx[i][t] = MIN(sim->fx[i][t], sim->uxmax_hard);
	sim->fx[i][t] = MAX(sim->fx[i][t], sim->uxmin_hard);

	sim->fy[i][t] = MIN(sim->fy[i][t], +sim->uymax_hard);
	sim->fy[i][t] = MAX(sim->fy[i][t], -sim->uymax_hard);

	/* non-negative speed */
	sim->fx[i][t] = MAX(sim->fx[i][t], -sim->vx[i][t]/sim->T);

	/* single-lane exceptions */
	if (sim->single_lane)
		sim->fy[i][t] = 0;

}

static void
determine_walls(sim_t *sim, int i, int t) {
	int j;
	double wall_x = fmod(sim->x[i][t] + 0.5*sim->roadlen_meters , sim->roadlen_meters);
	double wall_x_v = sim->vd[i];
	double wall_x_min_dx = 0.5*sim->roadlen_meters;

	double wall_y_dn = 0;
	double wall_y_up = sim->roadwid_meters;
	int leader = -1;

	/* obstacle-related walls */
	for (j=0; j < sim->n; j++) {
		if (j == i) continue;

 		double dx = circular_dx(sim, sim->x[j][t] - sim->x[i][t]);
		double dy = sim->y[j][t]-sim->y[i][t];
		double hl = 0.99*0.5*(sim->l[j]+sim->l[i]);
		int overlap_on_x = (dx >= -hl-0.5 && dx <= hl+0.5);
		int overlap_on_y = (fabs(sim->y[j][t]-sim->y[i][t]) <= 0.5*(sim->w[j]+sim->w[i])-0.1);
		int j_is_forward = (dx >= 0);
		
		if (j_is_forward && overlap_on_y && dx <= wall_x_min_dx) {
			wall_x = sim->x[j][t] - 0.5*sim->l[j] - 0.1;
			wall_x_v = sim->vx[j][t];
			wall_x_min_dx = dx;
			leader = j;
		}

		if (overlap_on_x) {
			if (dy < 0) {
				//wall_y_dn = MIN(wall_y_dn, WALL_DN(sim->y[j][t]+0.5*sim->w[j]));
				wall_y_dn = MAX(wall_y_dn, sim->y[j][t]+0.5*sim->w[j]+0.2);
			} else {
				//wall_y_up = MIN(wall_y_up, WALL_UP(sim->y[j][t]-0.5*sim->w[j]));
				wall_y_up = MIN(wall_y_up, sim->y[j][t]-0.5*sim->w[j]-0.2);
			}
		}

	}
	sim->wall_x[i][t] = wall_x;
	sim->wall_x_v[i][t] = wall_x_v;
	sim->wall_y_up[i][t] = wall_y_up;
	sim->wall_y_dn[i][t] = wall_y_dn;
	sim->leader[i][t] = leader;
}
