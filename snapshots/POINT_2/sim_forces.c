
#define WALL_DN(point, safety, v) U_lemma3(sim, -(safety)*MIN(0,v), sim->y[i][t]-0.5*sim->w[i] - (point), -sim->uymax_hard)
#define WALL_UP(point, safety, v) U_lemma3(sim, +(safety)*MAX(0,v), (point) - (sim->y[i][t]+0.5*sim->w[i]), -sim->uymax_hard)

static double
circular_dx(sim_t *sim, double dx) {
	double len = sim->roadlen_meters;
	if (fabs(dx) <= 0.5*len)
		return dx;
	else
		return (dx >= 0)?(dx-len):(dx+len);
}

static double
smooth_max(double x1, double x2, double z) {
	assert(z != 0);
	return log(exp(z*x1) + exp(z*x2))/z;
}

static double
smooth_step(double t, double z) {
	return erf(z * t);
	//return 0.5*(sqrt(4*z)*t/sqrt(1 + 4*z*t*t) + 1);
}

static double
smooth_pulse(double t, double a, double b, double z) {
	return smooth_step(t-a, z) - smooth_step(t-b, z);
}
static double
ftsx(sim_t *sim, int i, int t) {
	return smooth_max(sim->ftsx_lo, sim->ftsx_hi*(1 - exp(-sim->ftsx_zeta*(sim->vdx_effective[i]-sim->vx[i][t]))), 1.0);
}

static double
ftsy(sim_t *sim, int i, int t) {
	return sim->ftsy_hi*(2 / (1 + exp(-sim->ftsy_zeta*(sim->vdy_effective[i] - sim->vy[i][t]))) - 1);
}

static double
fmdl(sim_t *sim, int i, int t) {
	double vd_range = (sim->vd_meters_per_sec_hi - sim->vd_meters_per_sec_lo);
	double point_in_range = 0.5;
	double spread_factor = 1.0;
	double gravity_point;
	if (sim->virtual_lanes) {
		point_in_range = (sim->vd[i] - sim->vd_meters_per_sec_lo)/vd_range;
	}
	gravity_point = sim->w[i]/2 + (0 + point_in_range)*(sim->roadwid_meters - sim->w[i])*spread_factor;
	return sim->coeff_fmdl*(2 / (1 + exp(-sim->ftsy_zeta*(gravity_point - sim->y[i][t]))) - 1);
}

static double
U_lemma3(sim_t *sim, double v, double d, double ubar) {
	double nom, den; 
	double T = sim->T;
	double ret;

	nom = T*ubar- 2*v + sqrt(pow(T,2)*pow(ubar,2)-(4*T*ubar*v)+8*ubar*(T*v-d));
	den = 2*T;

	if (fpclassify((ret = nom/den)) == FP_NAN) {
		ret = ubar;
	}

	return ret;
}

static double
fca_mag(sim_t *sim, int i, int j, int t, int j_repels_i) {
	double xi = sim->x[i][t], xj = sim->x[j][t];
	double yi = sim->y[i][t], yj = sim->y[j][t];
	double li = sim->l[i], lj = sim->l[j];
	double wi = sim->w[i], wj = sim->w[j];
	double dx = circular_dx(sim, xj-xi);
	double dy = yj - yi;
	double mag = 0;
	
	// effective time-gaps
	double time_gap_x = sim->time_gap_x;
	double time_gap_y =	sim->time_gap_y;

	switch(sim->fca_method) {
		case FCA_0:
			do {
				double bufferx = time_gap_x * sim->vx[i][t];
				double buffery_up = time_gap_y * MIN(sim->vy[i][t], 0);
				double buffery_dn = time_gap_y * MAX(sim->vy[i][t], 0);

				mag = MIN(
							smooth_pulse(dx,-0.5*(li+lj) - bufferx, +0.5*(li+lj) + bufferx, sim->fcax_zeta), 
							smooth_pulse(dy,-0.5*(wi+wj)+buffery_up, +0.5*(wi+wj)+buffery_dn, sim->fcay_zeta)
						);
			}while (0);
			break;

		case FCA_1:
			do {
				double ax = 0.5*(li+lj);
				double ay = 0.5*(wi+wj);

				//double buffx = time_gap_x*(0.5*pow(sim->vx[i][t],2)/fabs(sim->uxmin_hard));
				//double buffy = time_gap_y*(0.5*pow(sim->vy[i][t],2)/fabs(sim->uymax_hard));
				double buffx = time_gap_x*sim->vx[i][t];
				double buffy = time_gap_y*fabs(sim->vy[i][t]);


				if (dx >= 0)
					ax += buffx;

				assert(dx >= 0);

				if ((sim->vy[i][t] >= 0 && yj > yi)  || (sim->vy[i][t] < 0 && yj < yi))
					ay += buffy;

				double xterm = pow(dx/ax, 2);
				double yterm = pow(dy/ay, 4);

				mag = 1.0/(1.0 + xterm + yterm);
			} while (0);
			break;

		case FCA_2:
			do {
				double ax = 0.5*(li+lj);
				double ay = 0.5*(wi+wj);

				//double bufferx = time_gap_x*(0.5*pow(sim->vx[i][t],2)/fabs(-2));
				//double buffery = time_gap_y*(0.5*pow(sim->vy[i][t],2)/fabs(-2));
				double bufferx = time_gap_x*sim->vx[i][t];
				double buffery = time_gap_y*fabs(sim->vy[i][t]);

				if (dx >= 0)
					ax += bufferx;

				assert(dx >= 0);

				if ((sim->vy[i][t] >= 0 && yj > yi)  || (sim->vy[i][t] < 0 && yj < yi))
					ay += buffery;

				double xterm = pow(dx/ax, 2);
				double yterm = pow(dy/ay, 4);
				
				mag = MIN(smooth_pulse(dx,-0.5*(li+lj)-bufferx, +0.5*(li+lj)+bufferx, 1), smooth_pulse(dy,-0.5*(wi+wj), +0.5*(wi+wj), 1)) 
					+ 1.0/(1.0 + xterm + yterm);

			} while (0);
			break;
		default:
			break;
	}

	return mag;
}

static int
fca(sim_t *sim, int i, int j, int t, double *fcax, double *fcay) {
	double dx, dy, mag, ang;
	double xmultiplier, ymultiplier;
	// i - me 
	// j - the obstacle

	*fcax = *fcay = 0;
	dx = circular_dx(sim, sim->x[j][t] - sim->x[i][t]);
	dy = sim->y[j][t] - sim->y[i][t];
	if (fabs(dy) >= 0.75*sim->roadwid_meters) {
		*fcax = *fcay = 0;
		return 0;
	}

	// calculate magnitude according to relative positioning
	if (dx >= 0) {
		// obstacle is in the front:
		// i accepts a backward repelent force from j 
		// magnitude depends on where j is located within i's aura
		mag = fca_mag(sim, i, j, t, 1);
		xmultiplier = sim->bwd_force_max_x;
		ymultiplier = sim->bwd_force_max_y;

	} else {
		// obstacle is in the back:
		// i accepts a forward push force from j
		// magnitude depends on where i is located within j's aura
		mag = fca_mag(sim, j, i, t, 0);
		xmultiplier = sim->fwd_force_max_x;
		ymultiplier = sim->fwd_force_max_y;
	}
	
	ang = atan2(dy, dx);
	*fcax = -cos(ang) * xmultiplier * mag;
	*fcay = -sin(ang) * ymultiplier * mag;

	return 1;
}

static int
crash(sim_t *sim, int i, int j, int t) {
	double dx = circular_dx(sim, sim->x[j][t] - sim->x[i][t]);
	double dy = sim->y[j][t] - sim->y[i][t];

	if (i == j)
		return 0;

	if (fabs(dx) <= 0.5*(sim->l[i]+sim->l[j]) && fabs(dy) <= 0.5*(sim->w[i]+sim->w[j])) {
		if (!sim->warmup) {
			sim->crashes++;
			if (t > 0 && ((sim->walls[i][t].x.leader == j) || (sim->walls[j][t].x.leader == i)))
				sim->crashes_on_leaders++;
		}
		
		return 1;
	}

	if (t > 0 && sim->ux[i][t-1] <= 0.95*sim->uxmin_hard)
		return 0;

	return 0;
}

static void
walls_init(sim_t *sim, int i, int t, walls_t *walls) {
	walls->x.position = fmod(sim->x[i][t] + 0.5*sim->roadlen_meters , sim->roadlen_meters);
	walls->x.distance = 0.5*sim->roadlen_meters;
	walls->x.velocity = sim->vd[i];
	walls->x.leader = -1;
	walls->x.bound = sim->uxmax_hard;

	walls->y.dn = 0;
	walls->y.up = sim->roadwid_meters;
	walls->y.up_j = walls->y.dn_j = -1;
	walls->y.bound_dn = WALL_DN(0, sim->safety_level_y, sim->vy[i][t]);
	walls->y.bound_up = WALL_UP(sim->roadwid_meters, sim->safety_level_y, sim->vy[i][t]);
}

static void
walls_update(sim_t *sim, int i, int j, int t, double fcax, double fcay, walls_t *walls) {
	if (j == i) 
		return;

	// clip forces within [-1, 1]
	fcax = MIN(1.0, MAX(-1.0, fcax));
	fcay = MIN(1.0, MAX(-1.0, fcay));

	// degree_x in [0, 1]: degree to which j is a front obstacle
	// degree_y in [0, 1]: degree to which j is a side obstacle
 	double degree_x = fabs(MIN(fcax, 0));
	double degree_y = fabs(fcay);

	// determine front bound
	double uxbound_v = MAX(0, sim->vx[i][t] - (1.0 - degree_x*sim->safety_level_x)*sim->vx[j][t]);
	double uxbound_d = circular_dx(sim, sim->x[j][t]-0.5*sim->l[j] - (sim->x[i][t]+0.5*sim->l[i]) - 0.0);
	double uxbound = U_lemma3(sim, +uxbound_v, uxbound_d, sim->uxmin_hard);
	uxbound = degree_x*uxbound + (1.0 - degree_x)*sim->uxmax_hard;

	if (uxbound < walls->x.bound) {
		walls->x.bound = uxbound;
		walls->x.leader = j;
		walls->x.distance = uxbound_d;
		walls->x.position = sim->x[j][t]-0.5*sim->l[j];
		walls->x.velocity = sim->vx[j][t];
	}

	if (walls->x.leader == j)
		goto bypass;

	// determine side bounds
	if (sim->y[j][t] < sim->y[i][t]) {
		double uybound_dn_y = sim->y[j][t]+0.5*sim->w[j];
		double uybound_dn_v = sim->vy[i][t] - sim->vy[j][t];
		double uybound_dn = WALL_DN(uybound_dn_y, degree_y*sim->safety_level_y, uybound_dn_v);
		uybound_dn = degree_y*uybound_dn + (1.0 - degree_y)*WALL_DN(0, sim->safety_level_y, sim->vy[i][t]);

		if (-uybound_dn > -walls->y.bound_dn && walls->y.bound_up >= -uybound_dn) {
			walls->y.bound_dn = uybound_dn;
			walls->y.dn_j = j;
			walls->y.dn = uybound_dn_y;
		}
		
	} else {
		double uybound_up_y = sim->y[j][t]-0.5*sim->w[j];
		double uybound_up_v = sim->vy[i][t] - sim->vy[j][t];
		double uybound_up = WALL_UP(uybound_up_y, degree_y*sim->safety_level_y, uybound_up_v);
		uybound_up = degree_y*uybound_up + (1.0 - degree_y)*WALL_UP(sim->roadwid_meters, sim->safety_level_y, sim->vy[i][t]);

		if (uybound_up < walls->y.bound_up && uybound_up >= -walls->y.bound_dn) {
			walls->y.bound_up = uybound_up;
			walls->y.up_j = j;
			walls->y.up = uybound_up_y;
		}
	}

bypass:
	return;
	#if 0
	double dx = circular_dx(sim, sim->x[j][t] - sim->x[i][t]);
	double dy = sim->y[j][t]-sim->y[i][t];


	double hl = 0.99*0.5*(sim->l[j]+sim->l[i]);
	double hw = 0.99*0.5*(sim->w[j]+sim->w[i]);

	int overlap_on_x;
	int overlap_on_y;
	overlap_on_x = (fabs(dx) <= hl+0.0);
	overlap_on_y = (fabs(dy) <= hw-0.1);

	overlap_on_y = (fabs(fcax) >= 0.4); 
	overlap_on_x = (fabs(fcay) >= 0.2);

	int j_is_forward = (dx >= 0);

	
	if (j_is_forward && overlap_on_y) {
		double uxbound_v = MAX(0, sim->vx[i][t] - (1.0-sim->safety_level_x)*sim->vx[j][t]);
		double uxbound_d = circular_dx(sim, sim->x[j][t]-0.5*sim->l[j] - (sim->x[i][t]+0.5*sim->l[i]));
		double uxbound = U_lemma3(sim, +uxbound_v, uxbound_d, sim->uxmin_hard);
		if (uxbound < walls->x.bound) {
			walls->x.bound = uxbound;
			walls->x.leader = j;
			walls->x.distance = dx;
			walls->x.position = sim->x[j][t]-0.5*sim->l[j];
			walls->x.velocity = sim->vx[j][t];
		}
	}

	if (overlap_on_x) {

		if (dy < 0) {
			double uybound_dn_y = sim->y[j][t]+0.5*sim->w[j];
			double uybound_dn_v = sim->vy[i][t];
			double uybound_dn = WALL_DN(uybound_dn_y, sim->safety_level_y, uybound_dn_v);

			if (-uybound_dn > -walls->y.bound_dn) {
				walls->y.bound_dn = uybound_dn;
				walls->y.dn_j = j;
				walls->y.dn = uybound_dn_y;
			}
			
		} else {
			double uybound_up_y = sim->y[j][t]-0.5*sim->w[j];
			double uybound_up_v = sim->vy[i][t];

			double uybound_up = WALL_UP(uybound_up_y, sim->safety_level_y, uybound_up_v);
			if (uybound_up < walls->y.bound_up) {
				walls->y.bound_up = uybound_up;
				walls->y.up_j = j;
				walls->y.up = uybound_up_y;
			}
		}
	}
	#endif

}

static void
determine_forces(sim_t *sim, int i, int t) {
	int j;
	double fcax = 0, fcay = 0;

	/* initalization */
	sim->vdx_effective[i] = MAX(0.1*sim->vd[i], MIN(sim->vd[i], sim->vdx_effective[i]+sim->T));
	sim->vdy_effective[i] = 0.5*sim->vdy_effective[i];

	walls_init(sim, i, t, &(sim->walls[i][t]));
	
 	sim->fx[i][t] = sim->fy[i][t] = 0;

	/* target-speed related forces */
	sim->fx[i][t] += ftsx(sim, i, t);
	sim->fy[i][t] += ftsy(sim, i, t);
	sim->fy[i][t] += fmdl(sim, i, t);

	/* obstacle-related forces */
	double fcax_sum = 0, fcay_sum =0;
	for (j=0; j < sim->n; j++) {
		if (j == i || circular_dx(sim, sim->x[j][t]-sim->x[i][t]) > sim->influence_radius_meters)
			continue;

		if (crash(sim, i, j, t)) {
			sim->crash[i][t] = 1;
			//if (!sim->warmup)
			//	fprintf(stderr, "crash at %d\n", t);
		}

		fca(sim, i, j, t, &fcax, &fcay);

		fcax_sum += fcax;
		fcay_sum += fcay;

		walls_update(sim, i, j, t, fcax, fcay, &(sim->walls[i][t]));
	}
	
	sim->fx[i][t] += sim->coeff_fcax * MAX(-1, MIN(1, fcax_sum));
	sim->fy[i][t] += sim->coeff_fcay * MAX(-1, MIN(1, fcay_sum));

	sim->wall_y_up[i][t] = sim->walls[i][t].y.up;
	sim->wall_y_dn[i][t] = sim->walls[i][t].y.dn;
	sim->wall_y_up_j[i][t] = sim->walls[i][t].y.up_j;
	sim->wall_y_dn_j[i][t] = sim->walls[i][t].y.dn_j;
}

static void
regulate_forces(sim_t *sim, int i, int t) {
	/* y wall */
	if (sim->dynamic_y_walls) {
		double uy_max = sim->walls[i][t].y.bound_up;
		double uy_min = sim->walls[i][t].y.bound_dn;
		if (uy_max >= -uy_min) {
			sim->fy[i][t] = MAX(sim->fy[i][t], -uy_min);
			sim->fy[i][t] = MIN(sim->fy[i][t], +uy_max);
		}
	}

	/* keeping vehicles within the road */
	sim->fy[i][t] = MAX(sim->fy[i][t], -WALL_DN(0, 1.05, sim->vy[i][t]));
	sim->fy[i][t] = MIN(sim->fy[i][t], +WALL_UP(sim->roadwid_meters, 1.05, sim->vy[i][t]));

	/* x wall */
	if (sim->dynamic_x_walls && sim->walls[i][t].x.leader != -1) 
		sim->fx[i][t] = MIN(sim->fx[i][t], sim->walls[i][t].x.bound);

	/* [umin, umax] ranges */
	sim->fx[i][t] = MIN(sim->fx[i][t], sim->uxmax_hard);
	sim->fx[i][t] = MAX(sim->fx[i][t], sim->uxmin_hard);

	sim->fy[i][t] = MIN(sim->fy[i][t], +sim->uymax_hard);
	sim->fy[i][t] = MAX(sim->fy[i][t], -sim->uymax_hard);

	/* non-negative speed */
	sim->fx[i][t] = MAX(sim->fx[i][t], -sim->vx[i][t]/sim->T);

	/* single-lane exceptions */
	if (sim->single_lane)
		sim->fy[i][t] = 0;
}
