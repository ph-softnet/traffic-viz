// random number generator from 'prng.js'
var llt_randgen = new Random(0);

function llt_random_seed(seed) {
	llt_randgen = new Random(seed);
}

function llt_random() {
	return llt_randgen.nextFloat();
}

// maximum displacement
var VxMax = 15;
var VxMin = 1;

var VyMax = 5;
var VyMin = -VyMax;

var MSG = "";

// circular road length
var sim = [[]];

function llt_get(t)
{
	return sim[t];
}

function llt_get_trj(j)
{
	var trj = {
		t: [],
		x: [],
		y: [],
		vx: [],
		vy: [],
		fx: [],
		fy: [],
		vd: [],
		vxvd: []
	};

	for (var t=0; t < sim.length; t++) {
		trj.t.push(t);
		trj.x.push(sim[t][j].x);
		trj.y.push(sim[t][j].y);
		trj.vd.push(sim[t][j].vd);
		trj.vx.push(sim[t][j].vx);
		trj.vy.push(sim[t][j].vy);
		trj.fx.push(sim[t][j].fx);
		trj.fy.push(sim[t][j].fy);
		trj.vxvd.push(sim[t][j].vx - sim[t][j].vd);
	}

	return trj;
}

function llt_initialize(conf)
{
	var j, car;

	// sim = [sim[0], ..., sim[conf.n] 
	// sim[t] = [car, car, ...]
	sim = [[]];

	for (j=0; j < conf.n; j++) {
		car = {};
		car.id = j;
		car.w = 0.6*conf.maxcarwid + Math.min(0.4, llt_random())*conf.maxcarwid;
		car.h = 0.5*car.w;

		car.x = 0.5*car.w + (0.4*conf.roadlen - 0.5*car.w) * llt_random();
		car.y = 0.5*car.h + llt_random() * (conf.roadwid - car.h);

		car.vd = 5 + 10*llt_random();

		car.vx = 0;
		car.vy = 0;

		car.fx = 0;
		car.fy = 0;

		sim[0].push(car);
	}
}

function llt_extend(conf)
{
	var tmax = sim.length;
	for (var t = tmax; t < tmax+conf.K; t++)
		llt_extend_step(conf);
}

function ellipsoid(dx, dy, ax, ay, zarg)
{
	let z = zarg || 1;
	return 1.0/(1.0 + Math.pow(dx/ax, 2*z) + Math.pow(dy/ay, 2*z));
	//return Math.exp(-(Math.pow(cdx(dx, roadlen), 2*z)/ax + Math.pow(dy,2*z)/ay));
}

function llt_extend_step(conf)
{
	const t = sim.length;
	const n = sim[0].length;
	const T = conf.T;
	const roadwid = conf.roadwid; 
	const roadlen = conf.roadlen;
	const a_frb = conf.frb;
	const a_fca = conf.fca;
	const a_fts_x = conf.fts_x;
	const a_fts_y = conf.fts_y;
	var j;

	// compute current state
	sim.push([]);
	for (j=0; j < n; j++) {
		sim[t].push({});

		// preserved attributes
		sim[t][j].w = sim[t-1][j].w;
		sim[t][j].h = sim[t-1][j].h;
		sim[t][j].vd = sim[t-1][j].vd;
		sim[t][j].id = sim[t-1][j].id;

		// new velocities
		sim[t][j].vx = sim[t-1][j].vx + T*sim[t-1][j].fx;
		sim[t][j].vy = sim[t-1][j].vy + T*sim[t-1][j].fy;

		/*
		if (sim[t][j].vx > VxMax) console.log("vx > " + VxMax);
		if (sim[t][j].vx < VxMin) console.log("vx < " + VxMin);
		if (sim[t][j].vy > VyMax) console.log("vy > " + VyMax);
		if (sim[t][j].vy < VyMin) console.log("vy < " + VyMin);
		*/

		// new x position (circular road)
		sim[t][j].x = sim[t-1][j].x + T*sim[t-1][j].vx + 0.5*Math.pow(T, 2)*sim[t-1][j].fx;
		sim[t][j].x = sim[t][j].x % roadlen;
		// new y position
		sim[t][j].y = sim[t-1][j].y + T*sim[t-1][j].vy + 0.5*Math.pow(T, 2)*sim[t-1][j].fy;
	}

	// compute forces acting on each vehicle j
	for (j=0; j < n; j++) {
		// initialize to zero
		sim[t][j].fx = 0;
		sim[t][j].fy = 0;


		// 1) push-pull forces (deactivated)
		for (i=0; i < n; i++) {
			if (i === j)
				continue;

			var xi = sim[t][i].x, xj = sim[t][j].x;
			var yi = sim[t][i].y, yj = sim[t][j].y;
			var wi = sim[t][i].w, wj = sim[t][j].w;
			var hi = sim[t][i].h, hj = sim[t][j].h;

			if (xi > xj)
				continue;

			// aura half-width (ahw) and half-height (ahh)
			var ahw = 0.5*wj + 0.5*wi + 0.7*(wj + wi)/2;
			var ahh = 0.5*hj + 0.5*hi + 0.3*(hj + hi)/2;
			
			//var ahw = 0.5*wj + 0.5*wi + 0.5*(wi+wj);
			//var ahh = 0.5*hj + 0.5*hi + 0.5*(hj+hi);
			//ahw *= 2;
			//ahh *= 2;

			// force magnitude & angle
			var mag = Math.exp(-(Math.pow(cdx(xi-xj, roadlen), 2)/ahw + Math.pow(yi-yj,2)/ahh));
			var ang = Math.atan2(yj-yi, cdx(xj-xi, roadlen));
			// force constituents
			var fxij = Math.cos(ang);
			var fyij = Math.sin(ang);
			//console.log("j:("+Math.round(xj)+","+Math.round(yj)+") i:("+Math.round(xi)+","+Math.round(yi) + ")"
			//	+ " ang " + (ang*180/Math.PI) + " fx " + fxij + " fy " + fyij + " mag " + mag);

			//sim[t][j].fx += a_fca * mag * fxij;
			//sim[t][j].fy += a_fca * mag * fyij;
		}

		// 1) horizontal time-gap forces coming from vehicle i
		for (i=0; i < n; i++) {
			if (i === j || sim[t][i].x <= sim[t][j].x)
				continue;

			let hij = 0.5*(sim[t][i].h + sim[t][j].h);
			let wij = 0.5*(sim[t][j].w + sim[t][i].w);

			// aura half-height
			let ahh = 0.5*hij;
			// aura half-width
			let xtg = 0.5;
			let ahw = xtg * sim[t][j].vx;

			sim[t][j].fx += -20.0 * ellipsoid((cdx(sim[t][j].x-sim[t][i].x, conf.roadlen)), Math.abs(sim[t][j].y-sim[t][i].y)-hij, ahw, ahh, 1);
		}

		// 2) vertical time-gap forces coming from vehicle i
		for (i=0; i < n; i++) {
			if (i === j)
				continue;

			let hij = 0.5*(sim[t][i].h + sim[t][j].h);
			let wij = 0.5*(sim[t][j].w + sim[t][i].w);

			// aura half-width
			let ytg = 0.5;
			let ahw = 0.5*wij;

			// aura half-height, force direction and magnitude
			var ahh = 0, dir = 0, mag = 0, dv;
			if (sim[t][i].y > sim[t][j].y) {
				// force points down
				dir = -1;
				dv = Math.max(0, sim[t][j].vy - sim[t][i].vy);
			} else {
				dir = +1;
				dv = Math.max(0, sim[t][i].vy - sim[t][j].vy);
			}
			ahh = ytg * dv;
			sim[t][j].fy += dir * 20.0 * ellipsoid((cdx(sim[t][j].x-sim[t][i].x, conf.roadlen)), Math.abs(sim[t][j].y-sim[t][i].y)-hij, ahw, ahh, 1);
		}

		// 3) target speed
		sim[t][j].fx += a_fts_x * f_vd(sim[t][j].vd, sim[t][j].vx);
		sim[t][j].fy += a_fts_y * f_vd(0, sim[t][j].vy);

		var upperlower = (sim[t][j].y > 0.5*roadwid)? -1: 1;

		// 3) center attractor
		sim[t][j].fy += upperlower*(0.1*a_rb(sim[t][j], roadwid, 0.25*roadwid));
	}

	// regulate forces
	for (j=0; j < n; j++) {
		sim[t][j].fx = Math.min(sim[t][j].fx, (VxMax - sim[t][j].vx)/T);
		sim[t][j].fx = Math.max(sim[t][j].fx, (VxMin - sim[t][j].vx)/T);

		sim[t][j].Uup = U_Lemma3(T, -4, sim[t][j].vy, roadwid-(sim[t][j].y+0.5*sim[t][j].h));
		sim[t][j].Udn = U_Lemma3(T, -4, -sim[t][j].vy, sim[t][j].y-0.5*sim[t][j].h);

		sim[t][j].fy = Math.max(sim[t][j].fy, -sim[t][j].Udn);
		sim[t][j].fy = Math.min(sim[t][j].fy, +sim[t][j].Uup);


		sim[t][j].fy = Math.max(sim[t][j].fy, -4);
		sim[t][j].fy = Math.min(sim[t][j].fy, +4);
	}
}

function U_Lemma3(T, ubar, v, d)
{
	var nom, den;

	nom = T*ubar-2*v+Math.sqrt((T**2 * ubar**2) - (4*T*ubar*v) + 8*ubar*(T*v-d));
	den = 2*T;

	return nom/den;
}

// a_rb = {1 when near the boundary, 0 otherwise}
function a_rb(car, roadwid, buffer)
{
	buffer = (buffer === undefined)? 0: buffer;

	var speed = car.vy;
	var carwidth = car.h;
	var sidewidth = 0.5*roadwid - 0.5*carwidth - buffer;
	var middle = 0.5*roadwid;
	var a = Q(-(car.y+sidewidth-middle)) + Q(car.y-sidewidth-middle);

	return a;
}


// circular distance metric
function cdx(d, roadlen)
{
	if (Math.abs(d) <= roadlen/2)
		return d;
	else
		return d >= 0? d-roadlen: d+roadlen;
}

// smooth step function used above
function U(z, smooth)
{
	smooth = ((smooth === undefined)? 1: smooth);
	return 1.0 / (1.0 + Math.exp(-2 * smooth * z));
}

// adjusted smooth step function
function Q(z, smooth)
{
	return Math.min(1, 2*U(z,smooth));
}

function f_vd(vd, vx) {
	var max_accel = 4;
	var max_decel = -10;
	var steep = 0.2;
	return Math.max(max_decel, max_accel*(1-Math.exp(-steep*(vd - vx))));
}

